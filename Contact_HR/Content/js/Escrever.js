﻿

$(document).ready(function () {
    $("#btnCancelar").click(function () {
        if (confirm("Confirma descartar as alterações?")) {
            window.location = '../Home/Index';
        }
        return false;
    });

});
function remainder(box, maxValue, id_span) {
    var conta = maxValue - box.length;

    document.getElementById(id_span).innerHTML = conta;
}
$(function () {
    $("textarea[maxlength]").bind('input propertychange', function () {
        var maxLength = $(this).attr('maxlength');
        if ($(this).val().length > maxLength) {
            $(this).val($(this).val().substring(0, maxLength));
        }
    });
});