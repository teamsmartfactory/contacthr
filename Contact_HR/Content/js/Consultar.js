﻿$(document).ready(function () {
    hideElement();
});

$("#searchBtn").click(function () {
    var key = $("#appendedInputButton").val();

    $("#messagePlace").empty();
    $("#responsePlace").empty();

    $('#responseContainer').hide();
    $('#divcontainer').hide();

    if (key !== undefined && key !== "") {
        $(".help-block").hide();
        $.ajax({
            url: '../Contact/ConsultarID',
            method: 'POST',
            data: {
                "messageKey": key
            },
            success: function (response) {
                json = JSON.parse(response);
                if (json.message !== undefined) {
                    if (json.response !== undefined) {
                        if (json.readStatus !== true) {

                            $("#messagePlace").append(json.message);
                            $("#responsePlace").append(json.response);

                            startRemainderTime(30);

                            $("#responseContainer").show();
                            $("#divcontainer").show();


                        } else {
                            //$('#myModalMessageRead').modal('show');
                            //hideElement();
                            alert('Messagem já lida')
                        }

                    } else {
                        //$('#myModalNoAnswer').modal('show');
                        //hideElement();
                        alert('Messagem sem resposta')
                    }
                } else {
                    //$('#myModalNotFound').modal('show');
                    //hideElement();
                    alert('Messagem não encontrada')
                }
            },
            ajax: false
        });
    } else {
        $('#myModalEmpty').modal('show');
    }
});

function startRemainderTime(sec) {
    var timer = setInterval(function () {
        $('#timer-container span.countdown').text(--sec);
        if (sec == 0) {
            window.location.replace('../Home/Index');
            clearInterval(timer);
        }
    }, 1000);
}

function hideElement() {
    $('#response').addClass('hidden');
    $('#responseContainer').hide();
    $('#appendedInputButton').val("");
    $('#appendedInputButton').focus();
    $('#divcontainer').hide();
}

function showElement() {
    $('.hidden').removeClass('hidden');
}