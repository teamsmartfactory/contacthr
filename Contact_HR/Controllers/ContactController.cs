﻿using Contact_HR.Models;
using Contact_HR.Models.DAO;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Contact_HR.Controllers
{
    public class ContactController : Controller
    {
        // GET: Contact
        public ActionResult Escrever()
        {
            return View();
        }
        public ActionResult Consultar()
        {
            return View();
        }
        [HttpPost]
        public String ConsultarID(string messageKey)
        {
            CONTACT c = new CONTACT();
            CONTACTDAO dao = new CONTACTDAO();

            c = dao.getContactByKey(messageKey);

            ContactDTO cdto = new ContactDTO
            {
                id = c.id,
                message = c.message,
                response = c.response,
                readStatus = c.readStatus
            };

            return JsonConvert.SerializeObject(cdto);
        }
    }
}