namespace Contact_HR.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("LANGUAGES")]
    public partial class LANGUAGE
    {
        public int id { get; set; }

        [StringLength(255)]
        public string country { get; set; }

        [StringLength(255)]
        public string locale { get; set; }

        [StringLength(255)]
        public string name { get; set; }
    }
}
