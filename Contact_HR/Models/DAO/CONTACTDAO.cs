﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Contact_HR.Models.DAO
{
    public class CONTACTDAO : BaseDAO<CONTACT>
    {
        public CONTACT getContactByKey(string key)
        {
            CONTACT c = new CONTACT();

            using (var context = new Context.Context())
            {
                c = context.CONTACTS.Where(p => p.messageKey == key).FirstOrDefault();
            }

            return (c);
        }

    }
}