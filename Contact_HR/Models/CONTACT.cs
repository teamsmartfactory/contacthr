namespace Contact_HR.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("CONTACTS")]
    public partial class CONTACT
    {
        public int id { get; set; }

        [StringLength(3000)]
        public string message { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? messageDate { get; set; }

        [StringLength(255)]
        public string messageKey { get; set; }

        public bool readStatus { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? replyDate { get; set; }

        [StringLength(3000)]
        public string response { get; set; }

        [StringLength(255)]
        public string subject { get; set; }

        public int? area { get; set; }

        public int? sitecode { get; set; }

        public virtual AREA AREA1 { get; set; }

        public virtual SITE SITE { get; set; }
    }
}
