namespace Contact_HR.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("AREAS")]
    public partial class AREA
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public AREA()
        {
            CONTACTS = new HashSet<CONTACT>();
        }

        public int id { get; set; }

        public bool? active { get; set; }

        [StringLength(255)]
        public string name { get; set; }

        [StringLength(255)]
        public string responsible { get; set; }

        public int? site_id { get; set; }

        public virtual SITE SITE { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CONTACT> CONTACTS { get; set; }
    }
}
