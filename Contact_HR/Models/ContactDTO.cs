﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Contact_HR.Models
{
    public class ContactDTO
    {
        public int id { get; set; }
        public string message { get; set; }
        public string response { get; set; }
        public bool readStatus { get; set; }
    }
}