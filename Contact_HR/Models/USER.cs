namespace Contact_HR.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("USERS")]
    public partial class USER
    {
        public int id { get; set; }

        public bool? active { get; set; }

        [StringLength(100)]
        public string email { get; set; }

        [StringLength(100)]
        public string firstName { get; set; }

        [StringLength(100)]
        public string lastName { get; set; }

        [StringLength(50)]
        public string password { get; set; }

        [StringLength(50)]
        public string resetHash { get; set; }

        [StringLength(255)]
        public string role { get; set; }

        [StringLength(3)]
        public string siteCode { get; set; }

        [StringLength(20)]
        public string username { get; set; }
    }
}
