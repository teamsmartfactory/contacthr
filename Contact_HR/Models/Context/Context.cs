namespace Contact_HR.Models.Context
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class Context : DbContext
    {
        public Context()
            : base("name=Prod")
        {
        }

        public virtual DbSet<AREA> AREAS { get; set; }
        public virtual DbSet<CONTACT> CONTACTS { get; set; }
        public virtual DbSet<LANGUAGE> LANGUAGES { get; set; }
        public virtual DbSet<SITE> SITES { get; set; }
        public virtual DbSet<USER> USERS { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AREA>()
                .Property(e => e.name)
                .IsUnicode(false);

            modelBuilder.Entity<AREA>()
                .Property(e => e.responsible)
                .IsUnicode(false);

            modelBuilder.Entity<AREA>()
                .HasMany(e => e.CONTACTS)
                .WithOptional(e => e.AREA1)
                .HasForeignKey(e => e.area);

            modelBuilder.Entity<CONTACT>()
                .Property(e => e.message)
                .IsUnicode(false);

            modelBuilder.Entity<CONTACT>()
                .Property(e => e.messageKey)
                .IsUnicode(false);

            modelBuilder.Entity<CONTACT>()
                .Property(e => e.response)
                .IsUnicode(false);

            modelBuilder.Entity<CONTACT>()
                .Property(e => e.subject)
                .IsUnicode(false);

            modelBuilder.Entity<LANGUAGE>()
                .Property(e => e.country)
                .IsUnicode(false);

            modelBuilder.Entity<LANGUAGE>()
                .Property(e => e.locale)
                .IsUnicode(false);

            modelBuilder.Entity<LANGUAGE>()
                .Property(e => e.name)
                .IsUnicode(false);

            modelBuilder.Entity<SITE>()
                .Property(e => e.city)
                .IsUnicode(false);

            modelBuilder.Entity<SITE>()
                .Property(e => e.country)
                .IsUnicode(false);

            modelBuilder.Entity<SITE>()
                .Property(e => e.estate)
                .IsUnicode(false);

            modelBuilder.Entity<SITE>()
                .Property(e => e.language)
                .IsUnicode(false);

            modelBuilder.Entity<SITE>()
                .Property(e => e.siteCode)
                .IsUnicode(false);

            modelBuilder.Entity<SITE>()
                .Property(e => e.url)
                .IsUnicode(false);

            modelBuilder.Entity<SITE>()
                .HasMany(e => e.AREAS)
                .WithOptional(e => e.SITE)
                .HasForeignKey(e => e.site_id);

            modelBuilder.Entity<SITE>()
                .HasMany(e => e.CONTACTS)
                .WithOptional(e => e.SITE)
                .HasForeignKey(e => e.sitecode);

            modelBuilder.Entity<USER>()
                .Property(e => e.email)
                .IsUnicode(false);

            modelBuilder.Entity<USER>()
                .Property(e => e.firstName)
                .IsUnicode(false);

            modelBuilder.Entity<USER>()
                .Property(e => e.lastName)
                .IsUnicode(false);

            modelBuilder.Entity<USER>()
                .Property(e => e.password)
                .IsUnicode(false);

            modelBuilder.Entity<USER>()
                .Property(e => e.resetHash)
                .IsUnicode(false);

            modelBuilder.Entity<USER>()
                .Property(e => e.role)
                .IsUnicode(false);

            modelBuilder.Entity<USER>()
                .Property(e => e.siteCode)
                .IsUnicode(false);

            modelBuilder.Entity<USER>()
                .Property(e => e.username)
                .IsUnicode(false);
        }
    }
}
